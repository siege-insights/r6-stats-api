# Java Spring API for the Rainbow Six: Siege Stats API
## Usage
This API implementation is intended to offer you an easy way to pull statistic data from the Rainbow Six: Siege stats API, which Ubisoft has published at: https://www.ubisoft.com/en-gb/game/rainbow-six/siege/stats

It provides the functionality to 
1. establish sessions (since an account is required for pulling data from this API)
1. Handle session issues
1. Provide entity mappings for the transport objects (models)
1. Multi-thread / asynchronous support
1. Spring / Spring-Boot support (for easy usage)

### Technical usage
#### Follow these steps if you want to use this API as Spring library in your p project
1. Add this library as dependency to your project.
   If you're using gradle add the following to the `gradle.build`:
   ```groovy
    repositories {
        mavenCentral()
        maven {
            url 'https://gitlab.com/api/v4/projects/22626322/packages/maven'
        }
    }
    dependencies {
        // ...
        // Change this to the newest version number
        // See: https://gitlab.com/siege-insights/r6-stats-api/-/packages
        implementation 'net.siegeinsights:r6sstatsapi:0.0.5-SNAPSHOT'
    }

    ```
1. Add the package as scan path to your project:
   ```java
   @SpringBootApplication(scanBasePackageClasses = {R6StatsAPIConfiguration.class})
    public class YourSpringApplication {...})
   ``` 

1. Use the API in your code - example: TODO, update with automatic session manager.
    ```java
    @SpringBootTest
    public class ApiExampleTest {
    
        public ApiExampleTest(UbiApiCommunicator communicator, UbiApiSessionManager sessionManager){
            // Autowired by spring
            this.communicator = communicator;
            this.sessionManager = sessionManager;
        }
    
        private final UbiApiCommunicator communicator;
        private final UbiApiSessionManager sessionManager;
    
        @Test
        public void doSomething() throws NoValidSessionException, UbiHardApiException, UbiApiErrorResponseException {
            sessionManager.addSession("User", "pass");
            communicator.getOverallQueueStats("21e4e8e4-b70a-4f8a-be4d-d0db7c8c9076", sessionManager.getNextValidSession().getUbiSession());
        }
    }

    
    ```


## API endpoints
TODO: Still under development...

This API supports the following endpoints:
* ...

## Disclaimer
*This project is not related to Ubisoft in any way, shape or form.* 
1. Whenever you use this API, you have to use it with respect to their services, since it pulls data from official Ubisoft servers.
1. We all want to have the ability to pull awesome statistic data from the Ubisoft servers. So don't make them disable this capability by abusing it.
1. **DO NOT ABUSE THIS TO COLLECT TONS OF DATA**
1. This is intended for small projects, which fetch data on a low frequent base.
**Do not refresh the data every few seconds...**


## License
This project is licensed under the AGPLv3, for details see [LICENSE](/LICENSE).

You can use this API for free, but have to publish the source-code of the software implementing it and name the author.

