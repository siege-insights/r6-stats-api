package net.siegeinsights.r6sstatsapi.session;

import lombok.Data;
import net.siegeinsights.r6sstatsapi.external.objects.respones.UbiCreateSessionResponse;

@Data
public class SessionEntry {
    private String username;
    private String password;
    private UbiCreateSessionResponse ubiSession;
    private boolean active = false;

    public boolean isUsable() {
        if (ubiSession == null) {
            return false;
        }
        return true;
    }

}
