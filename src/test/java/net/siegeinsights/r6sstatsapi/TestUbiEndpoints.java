package net.siegeinsights.r6sstatsapi;

import net.siegeinsights.r6sstatsapi.external.NoValidSessionException;
import net.siegeinsights.r6sstatsapi.external.UbiApiCommunicator;
import net.siegeinsights.r6sstatsapi.external.UbiApiRequestExecutor;
import net.siegeinsights.r6sstatsapi.external.objects.error.UbiApiErrorResponseException;
import net.siegeinsights.r6sstatsapi.external.objects.error.UbiApiException;
import net.siegeinsights.r6sstatsapi.external.objects.error.UbiHardApiException;
import net.siegeinsights.r6sstatsapi.session.UbiApiSessionManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutionException;

/**
 * Test Ubisoft endpoints
 */
@SpringBootTest(classes = {R6StatsAPIConfiguration.class})
@RunWith(SpringRunner.class)
class TestUbiEndpoints {

    public static final String TEST_UBI_UUID = "21e4e8e4-b70a-4f8a-be4d-d0db7c8c9076";

    @Test
    void contextLoads() {
    }

    @Autowired
    UbiApiCommunicator communicator;

    @Autowired
    private UbiApiSessionManager ubiApiSessionManager;

    @Autowired
    UbiApiRequestExecutor executor;

    @Value("#{environment.TEST_UBI_USERNAME}")
    private String testUbiUsername;

    @Value("#{environment.TEST_UBI_PASSWORD}")
    private String testUbiPassword;

    @BeforeEach
    public void prepareLogin() {
        if (ubiApiSessionManager.getSessions() == null || ubiApiSessionManager.getSessions().size() == 0) {
            Assertions.assertNotNull(testUbiUsername, "TEST_UBI_USERNAME is not configured as environment variable");
            Assertions.assertNotNull(testUbiPassword, "TEST_UBI_PASSWORD is not configured as environment variable");

            Assertions.assertTrue(ubiApiSessionManager.addSession(testUbiUsername, testUbiPassword),
                    "Expected to get a true as return from addSessions");
        }
    }

    @Test
    public void getProfile() throws ExecutionException, InterruptedException {
        final var response = executor.getProfile().get();
        Assertions.assertNotNull(response);
        Assertions.assertTrue(response.getEmail().length() > 1, "User has no email set which should always be set");
    }

    @Test
    public void getPlayer() throws ExecutionException, InterruptedException {
        final var response = executor.getPlayer(TEST_UBI_UUID).get();
        Assertions.assertNotNull(response, "The response is null but a result was expected.");
        Assertions.assertEquals(1, response.getPlayers().size(), "Size of players is not 1");
        final var firstPlayer = response.getPlayers().entrySet().stream().findFirst().get().getValue();
        Assertions.assertTrue(firstPlayer.getMmr() > 0, "MMR is not present");
    }


    @Test
    public void getSeasonalSummary() throws NoValidSessionException, UbiHardApiException, UbiApiErrorResponseException {
        final var seasonalSummary = communicator.getSeasonalSummary(TEST_UBI_UUID, ubiApiSessionManager.getNextValidSession().getUbiSession());
        Assertions.assertNotNull(seasonalSummary);
        // TODO check some fields in here for correct values
    }

    @Test
    public void getQueues() throws UbiApiException, NoValidSessionException, ExecutionException, InterruptedException {
        final var result = executor.getOverallRankQueue(TEST_UBI_UUID).get().getResults().get(TEST_UBI_UUID);
        Assertions.assertNotNull(result);
        Assertions.assertTrue(result.getRankedPvpKills() > 0);
        Assertions.assertTrue(result.getRankedPvpTimePlayed() > 100);
        Assertions.assertTrue(result.getRankedPvpDeaths() > 0);
        Assertions.assertTrue(result.getRankedPvpMatchesLost() > 0);
        Assertions.assertTrue(result.getRankedPvpMatchesWon() > 0);
    }


}
